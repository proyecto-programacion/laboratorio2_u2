// Ejercicio 1: función imprime hola y el nombre de usuario.
function hellonombreUsuario(){
	// se crea la variable y se le pide que ingrese el nombre al usuario.
	let nombre = prompt(" Ingresar nombre: ");
	// se imprime el saludo.
	alert("Hola " + nombre);
}

// Ejercicio 2: función que suma dos números.
function sumar() {
	// define variables y realiza la suma.
	let a = prompt(" Ingresar el primer número: ");
	let b = prompt(" Ingresar el segundo número: ");
	// se utiliza parseInt para obtener el valor de la variable.
	let suma = parseInt(a) + parseInt(b);
	// muestra respuesta en popup.
	alert(suma);
}

// Ejercicio 3: función que calcula numero mayor.
function NumeroMayorDe2() {
	// define variables y realiza la suma.
	let a = prompt(" Ingresar el primer número: ");
	let b = prompt(" Ingresar el segundo número: ");
	// se obtienen los valores de las variables.
	let numero1 =parseInt(a);
	let numero2 = parseInt(b);
	// se ve cual es mayor y se imprime.
	if(numero1 > numero2){
		alert("El numero mayo ingresado es: " + numero1);
	}
	else if(numero1 < numero2){
		alert("El numero mayo ingresado es: " + numero2);
	}
	else{
		alert("Los números son iguales");
	}
}
// Ejercicio 4: función que calcula numero mayor.
function NumeroMayor() {
	// define variables y realiza la suma.
	let a = prompt(" Ingresar el primer número: ");
	let b = prompt(" Ingresar el segundo número: ");
	let c = prompt(" Ingresar el tercer número: ");
	// se obtienen los valores de las variables.
	let numero1 =parseInt(a);
	let numero2 = parseInt(b);
	let numero3 = parseInt(c);
	
	// se ve cual es mayor y se imprime.
	if(numero1 > numero2 && numero1 > numero3){
		alert("El numero mayo ingresado es: " + numero1);
	}
	else if(numero2 > numero1 && numero2 > numero3){
		alert("El numero mayo ingresado es: " + numero2);
	}
	else if(numero3 > numero1 && numero3 > numero2){
		alert("El numero mayo ingresado es: " + numero3);
	}
	else{
		alert("Los números son iguales");
	}
}

// Ejercicio 5: Función para ver si un número es divisible por 2
function divisible() {
	// solicita un número.
	let numero = prompt("Introduce un número entero");
	
	if((numero%2) == 0){
		alert("El numero " + numero + " es divisible por 2");
	}
	else{
		alert("El numero " + numero + " no es divisible por 2");
	}
}

// Ejercicio 6: Función para contar letras "a" en una frase.
function ContarA(){
	// se crea la variable y se le pide que  el usuario la ingrese.
	let frase = prompt(" Ingresar frase: ");
	// se saca el largo de la frase.
    let largo = frase.length;
    // se crea el contador.
    let contador = 0;
    
    // ciclo para recorrer la frase.
	for(let i = 0; i < largo; i++){
		// Cuando hay una letra "a" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "a" || frase[i] == "A") {
			contador= contador + 1;
		}
	}
	// se muestra cuantas "a" hay en la frase.
	alert("Hay " + contador + " letra o letras (a) en la frase: " + frase);
}

// Ejercicio 7: Función para contar las vocales en una frase.
function ContarVocales(){
	// se crea la variable y se le pide que  el usuario la ingrese.
	let frase = prompt(" Ingresar frase: ");
	// se saca el largo de la frase.
    let largo = frase.length;
    // se crea el contador.
    let contador = 0;
    
    // ciclo para recorrer la frase.
	for(let i = 0; i < largo; i++){
		// Cuando hay una letra "a" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "a" || frase[i] == "A") {
			contador = contador + 1;
		}
		// Cuando hay una letra "e" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "e" || frase[i] == "E") {
			contador = contador + 1;
		}
		// Cuando hay una letra "i" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "i" || frase[i] == "I") {
			contador = contador + 1;
		}
		// Cuando hay una letra "o" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "o" || frase[i] == "O") {
			contador = contador + 1;
		}
		// Cuando hay una letra "u" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "u" || frase[i] == "U") {
			contador = contador + 1;
		}
	}
	// se muestra cuantas vocales hay en la frase.
	alert("Hay " + contador + " vocales en la frase: " + frase);
}


// Ejercicio 8: Función para contar cuantas veces estan las vocales presentes en una frase.
function ContarCantidadVocales(){
	// se crea la variable y se le pide que  el usuario la ingrese.
	let frase = prompt(" Ingresar frase: ");
	// se saca el largo de la frase.
    let largo = frase.length;
    // se crea el contador.
    let contadorA = 0;
    let contadorE = 0;
    let contadorI = 0;
    let contadorO = 0;
    let contadorU = 0;
    
    // ciclo para recorrer la frase.
	for(let i = 0; i < largo; i++){
		// Cuando hay una letra "a" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "a" || frase[i] == "A") {
			contadorA = contadorA + 1;
		}
		// Cuando hay una letra "e" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "e" || frase[i] == "E") {
			contadorE = contadorE + 1;
		}
		// Cuando hay una letra "i" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "i" || frase[i] == "I") {
			contadorI = contadorI + 1;
		}
		// Cuando hay una letra "o" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "o" || frase[i] == "O") {
			contadorO = contadorO + 1;
		}
		// Cuando hay una letra "u" sea mayuscula o minuscula el contador aumenta en 1.
		if(frase[i] == "u" || frase[i] == "U") {
			contadorU = contadorU + 1;
		}
	}
	// se muestra cuantas vocales hay en la frase.
	alert("Hay " + contadorA + " vocales (a) en la frase: " + frase +
          "\n Hay " + contadorE + " vocales (e) en la frase: " + frase +
          "\n Hay " + contadorI + " vocales (i) en la frase: " + frase +
          "\n Hay " + contadorO + " vocales (o) en la frase: " + frase +
          "\n Hay " + contadorU + " vocales (u) en la frase: " + frase);
}

// Ejercicio 9: Funcion para cambiar de pesos a dolar.
function cambioDolar(){
	// se define la variable y se pide ingresar por el usuario.
	let pesos = prompt("Ingresar la cantidad de pesos: ");
	let cambiar = parseInt(pesos);
	
	// formula para transformar de pesos a dolar.
	let dolares = cambiar/850;
	
	// se muestra la cantidad de dolar.
	alert(cambiar + " pesos son: " + dolares + " dolares"); 

}

// Ejercicio 9: Funcion para cambiar de dolar a pesos
function cambioPesos(){
	// se define la variable y se pide ingresar por el usuario.
	let dolar = prompt("Ingresar la cantidad de dolares: ");
	let cambiar = parseInt(dolar);
	
	// fromula para transformar de dolar a pesos.
	let pesos = cambiar*850;
	
	// se muestra la cantidad de pesos.
	alert(cambiar + " dolares son: " + pesos + " pesos"); 

}

// Ejercicio 10: Lanzar un dado
function dado() {
	// creamos la variable alatoria del 1 al 6.
	let lanzar = Math.floor((Math.random() * 6)+1);
    
	// se crea un switch para ver cada caso.
	switch (lanzar){
		// si el numero sale 1 se muestra la cara 1 del dado.
		case 1:
			document.getElementById('dados').src = "dados/1.png";
			break;
		// si el numero sale 2 se muestra la cara 2 del dado.
		case 2:
			document.getElementById('dados').src = "dados/2.png";
			break;
		// si el numero sale 3 se muestra la cara 3 del dado.
		case 3:
			document.getElementById('dados').src = "dados/3.png";
			break;
		// si el numero sale 4 se muestra la cara 4 del dado.
		case 4:
			document.getElementById('dados').src = "dados/4.png";
			break;
		// si el numero sale 5 se muestra la cara 5 del dado.
		case 5:
			document.getElementById('dados').src = "dados/5.png";
			break;
		// si el numero sale 6 se muestra la cara 6 del dado.
		case 6:
			document.getElementById('dados').src = "dados/6.png";
			break;
    }
}

// damosla variable contador para ir cambiando las imagenes.
let contador = 1;
// Ejercicio 11: Funcion para avanzar a la siguiente imagen.
function siguiente(){
	// cuando se apreta el boton siguiente aumenta el contador en 1.
	contador = contador +1;
	// si el contador es 1 se muestra la imagen 1.
	if(contador == 1){
		document.getElementById('imagenes').src = "fotos/1.jpg";
    }
    // si el contador es 2 se muestra la imagen 2.
   	else if(contador == 2){
		document.getElementById('imagenes').src = "fotos/2.jpg";
	}
	// si el contador es 3 se muestra la imagen 3.
	else if(contador == 3){
		document.getElementById('imagenes').src = "fotos/3.jpeg";
	}
	// si el contador es 4 se muestra la imagen 4.
	else if(contador == 4){
		document.getElementById('imagenes').src = "fotos/4.jpg";
	}
	// si el contador es 5 se muestra la imagen 5.
	else{
		document.getElementById('imagenes').src = "fotos/5.jpg";
		// caundo el contador llega 5 se reinicia el contador a 0.
		contador = 0;
	}
}

// Ejercicio 11: Funcion para retroceder a la anterior imagen.
function anterior(){
	// cuando se apreta el boton anterior disminuye el contador en 1.
	contador = contador - 1;
	// si el contador es 1 se muestra la imagen 1.
	if(contador == 1){
		document.getElementById('imagenes').src = "fotos/1.jpg";
    }
    // si el contador es 2 se muestra la imagen 2.
   	else if(contador == 2){
		document.getElementById('imagenes').src = "fotos/2.jpg";
	}
	// si el contador es 3 se muestra la imagen 3.
	else if(contador == 3){
		document.getElementById('imagenes').src = "fotos/3.jpeg";
	}
	// si el contador es 4 se muestra la imagen 4.
	else if(contador == 4){
		document.getElementById('imagenes').src = "fotos/4.jpg";
	}
	// si el contador es 5 se muestra la imagen 5.
	else{
		document.getElementById('imagenes').src = "fotos/5.jpg";
		// caundo el contador llega 0 se reinicia el contador a 5.
		contador = 5;
	}
	
}

